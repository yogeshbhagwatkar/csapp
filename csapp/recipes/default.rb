#
# Cookbook:: csapp
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

include_recipe 'csapp::setup'

directory '/tmp/project' do
  owner 'ubuntu'
  group 'ubuntu'
  mode  '755'
  action :create
end

bash "Run the configuraiton for eve" do
  user "root"
  code <<-EOH
   sudo su evesleep
   aws s3 cp s3://yogbucket/cs-app.zip   "/tmp/project"
  EOH
end

execute 'extract directory' do
  command 'sudo unzip /tmp/project/cs-app.zip -d /tmp/project/'
  user 'ubuntu'
end

directory '/opt/eve' do
  owner 'ubuntu'
  group 'ubuntu'
  mode  '755'
  action :create
end


execute 'copy data dir.' do
  command 'sudo rsync -arz /tmp/project/cs-app  /opt/eve/'
  user 'ubuntu'
end

directory "/tmp/project/cs-app" do
  recursive true
  action :delete
end

execute 'change the permission' do
  command 'chown evesleep. /opt/eve -R'
  user 'root'
end

bash "Run the configuraiton for eve" do
  user "root"
  code <<-EOH
   sudo su evesleep
   cd /opt/eve/cs-app
   virtualenv -p python3 csapp
   chown evesleep. /opt/eve -R
   source csapp/bin/activate
   pip3 install -r req.txt

   cd /opt/eve/cs-app/csapp/bin
   pip3 install gunicorn
   chown evesleep. /opt/eve -R
  EOH
end

cookbook_file '/etc/systemd/system/app.service' do
  source 'app.service'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

execute "reload service" do
  command "systemctl daemon-reload"
end

execute "start app service" do
  command "sudo systemctl start app"
end

execute "status of App service" do
  command "systemctl status app.service"
end

cookbook_file '/etc/nginx/sites-enabled/cssapp.conf' do
  source 'cssapp.conf'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

cookbook_file '/etc/nginx/sites-enabled/default' do
  source 'default'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

execute 'check nginx syntax' do
  command 'nginx -t'
  user 'root'
end

service 'nginx' do
  action :restart
end

