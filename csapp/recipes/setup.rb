execute 'apt-get update' do
  command 'apt-get update'
  user 'root'
  action :run
end

execute 'apt-get upgrade' do
  command 'apt-get upgrade -y -qq'
  user 'root'
  action :run
end

execute 'Install python-pip' do
  command 'apt install -y python-pip'
  user 'root'
end

execute 'Install python-pip' do
  command 'pip install --upgrade pip'
  user 'root'
end

execute "Install python-pip3" do
  command "apt install -y python3-pip"
  user "root"
end

package "zip" do
  action :install
end

package "unzip" do
  action :install
end

package 'virtualenv' do
  action :install
end

package 'nginx' do
  action :install
end

service 'nginx' do
  action [:start, :enable]
end

execute 'install aws_cli' do
  command 'pip install awscli'
  user 'root'
end

execute 'Upgrade awscli' do
  command 'pip install --upgrade awscli'
  user 'root'
end

group 'evesleep' do
end

user 'evesleep' do
  password '$1$tata$.y/rEeq60zUAVqnFPaSiZ/'
  group 'evesleep'
  shell '/bin/bash'
  home '/home/evesleep'
  manage_home true
  action :create
end




